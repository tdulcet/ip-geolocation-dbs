#!/usr/bin/env python3
# Teal Dulcet

# Run: python3 update.py <IP version> <input CSV file> <output CSV file>

import csv
import ipaddress
import os
import sys
import tempfile

if len(sys.argv) != 4:
	print(f"Usage: {sys.argv[0]} <IP version> <input CSV file> <output CSV file>", file=sys.stderr)
	sys.exit(1)

version = int(sys.argv[1])

if version not in {4, 6}:
	sys.exit(1)

with open(sys.argv[2], newline="", encoding="utf-8") as csvfile:
	reader = csv.reader(csvfile)
	with tempfile.NamedTemporaryFile("w", dir=os.path.dirname(sys.argv[3]), newline="", encoding="utf-8", delete=False) as csvfile:
		writer = csv.writer(csvfile, delimiter="\t", lineterminator="\n", quoting=csv.QUOTE_NONE)
		for row in reader:
			if ipaddress.ip_address(row[1]).version == version and row[2] != "ZZ":
				writer.writerow([f"{int(ipaddress.ip_address(x)):x}" for x in row[:2]] + row[2:])
		os.replace(csvfile.name, sys.argv[3])

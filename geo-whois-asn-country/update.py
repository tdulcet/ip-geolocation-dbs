#!/usr/bin/env python3
# Teal Dulcet

# Run: python3 update.py <input CSV file> <output CSV file>

import csv
import os
import sys
import tempfile

if len(sys.argv) != 3:
	print(f"Usage: {sys.argv[0]} <input CSV file> <output CSV file>", file=sys.stderr)
	sys.exit(1)

with open(sys.argv[1], newline="", encoding="utf-8") as csvfile:
	reader = csv.reader(csvfile)
	with tempfile.NamedTemporaryFile("w", dir=os.path.dirname(sys.argv[2]), newline="", encoding="utf-8", delete=False) as csvfile:
		writer = csv.writer(csvfile, delimiter="\t", lineterminator="\n", quoting=csv.QUOTE_NONE)
		for row in reader:
			writer.writerow([f"{int(x):x}" for x in row[:2]] + row[2:])
		os.replace(csvfile.name, sys.argv[2])

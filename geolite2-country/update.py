#!/usr/bin/env python3
# Teal Dulcet

# Run: python3 update.py <input CSV block file> <output CSV file> <input CSV location files>...

import csv
import ipaddress
import os
import sys
import tempfile

if len(sys.argv) <= 3:
	print(f"Usage: {sys.argv[0]} <input CSV block file> <output CSV file> <input CSV location files>...", file=sys.stderr)
	sys.exit(1)

locales = []
locations = {}

for file in sys.argv[3:]:
	locale = os.path.splitext(os.path.basename(file))[0].split("-", 3)[-1]
	locales.append(locale)
	with open(file, newline="", encoding="utf-8") as csvfile:
		reader = csv.DictReader(csvfile)
		location = {}
		locations[locale] = location
		for row in reader:
			location[row["geoname_id"]] = row

print(locales)
locale = "en"

with open(sys.argv[1], newline="", encoding="utf-8") as csvfile:
	reader = csv.DictReader(csvfile)
	with tempfile.NamedTemporaryFile("w", dir=os.path.dirname(sys.argv[2]), newline="", encoding="utf-8", delete=False) as csvfile:
		writer = csv.writer(csvfile, delimiter="\t", lineterminator="\n", quoting=csv.QUOTE_NONE)
		for row in reader:
			key = row["geoname_id"]
			if key:
				network = ipaddress.ip_network(row["network"])
				ip = [f"{int(network[0]):x}", f"{int(network[-1]):x}"]
				location = locations[locale][key]
				writer.writerow(ip + ([location["country_iso_code"]] if location else [""]))
		os.replace(csvfile.name, sys.argv[2])

#!/usr/bin/env python3
# Teal Dulcet

# Run: python3 update.py <input CSV block file> <output CSV file> <input CSV location files>...

import csv
import ipaddress
import os
import sys
import tempfile
from contextlib import ExitStack

if len(sys.argv) <= 3:
	print(f"Usage: {sys.argv[0]} <input CSV block file> <output CSV file> <input CSV location files>...", file=sys.stderr)
	sys.exit(1)

locales = []
locations = {}

for file in sys.argv[3:]:
	locale = os.path.splitext(os.path.basename(file))[0].split("-", 3)[-1]
	locales.append(locale)
	with open(file, newline="", encoding="utf-8") as csvfile:
		reader = csv.DictReader(csvfile)
		location = {}
		locations[locale] = location
		for row in reader:
			location[row["geoname_id"]] = row

print("Locales:", locales)

with open(sys.argv[1], newline="", encoding="utf-8") as csvfile:
	reader = csv.DictReader(csvfile)
	root, ext = os.path.splitext(sys.argv[2])
	with ExitStack() as stack:
		csvfiles = [
			stack.enter_context(
				tempfile.NamedTemporaryFile("w", dir=os.path.dirname(sys.argv[3]), newline="", encoding="utf-8", delete=False)
			)
			for locale in locales
		]
		writers = [
			csv.writer(csvfile, delimiter="\t", lineterminator="\n", quotechar=None, quoting=csv.QUOTE_NONE) for csvfile in csvfiles
		]
		for row in reader:
			key = row["geoname_id"]
			if key:
				network = ipaddress.ip_network(row["network"])
				ip = [f"{int(network[0]):x}", f"{int(network[-1]):x}"]
				for idx, locale in enumerate(locales):
					location = locations[locale][key] if key in locations[locale] else locations["en"][key]
					writers[idx].writerow(
						ip
						+ (
							[location[x] for x in ("country_iso_code", "subdivision_1_name", "subdivision_2_name", "city_name")]
							if location
							else [""] * 4
						)
						+ [float(row[x]) if row[x] else "" for x in ("latitude", "longitude")]
					)
		for idx, locale in enumerate(locales):
			os.replace(csvfiles[idx].name, f"{root}-{locale}{ext}")

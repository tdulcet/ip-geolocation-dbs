#!/usr/bin/env python3
# Teal Dulcet

# Run: python3 update.py <IP version> <input CSV file> <output CSV file>

import csv
import ipaddress
import os
import sys
import tempfile

if len(sys.argv) != 4:
	print(f"Usage: {sys.argv[0]} <IP version> <input CSV file> <output CSV file>", file=sys.stderr)
	sys.exit(1)

version = int(sys.argv[1])

if version not in {4, 6}:
	sys.exit(1)

with open(sys.argv[2], newline="", encoding="utf-8") as csvfile:
	reader = csv.DictReader(csvfile)
	with tempfile.NamedTemporaryFile("w", dir=os.path.dirname(sys.argv[3]), newline="", encoding="utf-8", delete=False) as csvfile:
		writer = csv.writer(csvfile, delimiter="\t", lineterminator="\n", quoting=csv.QUOTE_NONE)
		for row in reader:
			if ipaddress.ip_address(row["end_ip"]).version == version:
				writer.writerow([f"{int(ipaddress.ip_address(row[x])):x}" for x in ("start_ip", "end_ip")] + [row["country"]])
		os.replace(csvfile.name, sys.argv[3])

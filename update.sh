#!/bin/bash

# Teal Dulcet
# Update geolocation databases
# ./update.sh

# ip2location.com tokens
# The download limit is very strict, so need two tokens
# TOKEN1=
# TOKEN2=
# maxmind.com license key
# KEY=
# ipinfo.io token
# TOKEN=

# Output CSV files
OUTPUT4=ipv4.tsv
OUTPUT6=ipv6.tsv

# GitLab repository base URL
URL=https://gitlab.com/tdulcet/ip-geolocation-dbs

# Directories
DIRS=(
	{geo-whois-asn,iptoasn,ipinfo}-country
	dbip-{country,city}
	ip2location-{country,city}
	geolite2-{country,city}
)

# Files
FILES=(
	geo-whois-asn-country-ipv{4,6}-num.csv
	ip2country-v{4-u32,6}.tsv.gz country.csv.gz
	dbip-{country,city}-lite-"$(date -u '+%Y-%m')".csv.gz
	IP2LOCATION-LITE-DB{1,5}{,.IPV6}.CSV.ZIP
	GeoLite2-{Country-CSV_*/GeoLite2-Country,City-CSV_*/GeoLite2-City}-Blocks-IPv{4,6}.csv
)

# set -e

if [[ $# -ne 0 ]]; then
	echo "Usage: $0" >&2
	exit 1
fi

# wget arguments
wget_args=(-nv -T 30 --retry-connrefused -c --content-disposition --compression auto)

# curl arguments
curl_args=(--no-progress-meter -L --connect-timeout 30 --retry 20 --retry-connrefused --compressed) # -OJ

# outputupdate <file>
outputupdate() {
	date -ud "@$(stat -c %Y -- "$1")" +%F
}

# Auto-scale number to unit
# Adapted from: https://github.com/tdulcet/Numbers-Tool/blob/master/numbers.cpp
# outputunit <number> [scale_base]
# scale_base:
	# 0=scale_IEC_I
	# 1=scale_SI
outputunit() {
	echo "$*" | awk 'BEGIN { suffix[0]=""; suffix[1]="K"; suffix[2]="M"; suffix[3]="G"; suffix[4]="T"; suffix[5]="P"; suffix[6]="E"; suffix[7]="Z"; suffix[8]="Y"; suffix[9]="R"; suffix[10]="Q" } function abs(x) { return x<0 ? -x : x } { number=$1; if ($2) scale_base=1000; else scale_base=1024; power=0; while (abs(number)>=scale_base) { ++power; number /= scale_base } anumber=abs(number); anumber += anumber<10 ? 0.0005 : (anumber<100 ? 0.005 : (anumber<1000 ? 0.05 : 0.5)); if (number!=0 && anumber<1000 && power>0) { str=sprintf("%'"'"'.15g", number); alength=5 + (number<0 ? 1 : 0); if (length(str) > alength) { prec=anumber<10 ? 3 : (anumber<100 ? 2 : 1); str=sprintf("%'"'"'.*f", prec, number) } } else str=sprintf("%'"'"'.0f", number); if (power<length(suffix)) str=str suffix[power]; else str=str "(error)"; if (! $2  && power>0) str=str "i"; print str }'
}

# outputfile <dir> <files>...
outputfile() {
	for i in "${@:2}"; do
		size=$(du -b -- "$i" | awk '{ print $1 }')
		echo -n "⬇️ **[$i]($URL/-/raw/main/$1/$i?inline=false)**<br>$(outputunit "$size" 0)B ($(outputunit "$size" 1)B) – $(printf "%'d" "$(wc -l -- "$i" | awk '{ print $1 }')") rows – $(printf "%'d" "$(cut -f 3 "$i" | sort -u | wc -l)") unique countries<br><small><details><summary>Checksums (click to show)</summary><pre>MD5: $(md5sum -- "$i" | awk '{ print $1 }')<br>SHA1: $(sha1sum -- "$i" | awk '{ print $1 }')<br>SHA256: $(sha256sum -- "$i" | awk '{ print $1 }')</pre></details></small>"
	done
}

# headtail <file>
headtail() {
	head "$1"
	echo "..."
	tail "$1"
}

(
	set -e
	trap 'echo "::error::GeoFeed + Whois + ASN failed to update"' ERR

	DIR=${DIRS[0]}
	FILE4=${FILES[0]}
	FILE6=${FILES[1]}

	echo -e "\nGeoFeed + Whois + ASN\n"

	mkdir -p "$DIR"
	cd "$DIR"

	echo -e "\n\tDownloading “${FILE4}”\n"
	wget "${wget_args[@]}" "https://github.com/sapics/ip-location-db/raw/main/geo-whois-asn-country/$FILE4"

	# head "$FILE4"

	echo -e "\n\tDownloading “${FILE6}”\n"
	wget "${wget_args[@]}" "https://github.com/sapics/ip-location-db/raw/main/geo-whois-asn-country/$FILE6"

	# head "$FILE6"

	echo -e "\n\tIPv4 database “$OUTPUT4”\n"
	python3 -X dev update.py "$FILE4" $OUTPUT4

	head $OUTPUT4

	echo -e "\n\tIPv6 database “$OUTPUT6”\n"
	python3 -X dev update.py "$FILE6" $OUTPUT6

	head $OUTPUT6

	echo "| [GeoFeed + Whois + ASN](#geofeed-whois-asn-database) | 🅭🄍<br>CC0 1.0 | Country | Daily<br>IPv4: $(outputupdate "$FILE4")<br>IPv6: $(outputupdate "$FILE6") | $(outputfile "$DIR" $OUTPUT4) | $(outputfile "$DIR" $OUTPUT6) |" >table.txt
)

(
	set -e
	trap 'echo "::error::iptoasn.com failed to update"' ERR

	DIR=${DIRS[1]}
	FILE4=${FILES[2]}
	FILE6=${FILES[3]}

	echo -e "\niptoasn.com\n"

	cd "$DIR"

	echo -e "\n\tDownloading “${FILE4}”\n"
	wget "${wget_args[@]}" "https://iptoasn.com/data/$FILE4"
	echo -e "\n\tDecompressing the files\n"
	gzip -dkfv "$FILE4"

	head "${FILE4%.gz}"

	echo -e "\n\tDownloading “${FILE6}”\n"
	wget "${wget_args[@]}" "https://iptoasn.com/data/$FILE6"
	echo -e "\n\tDecompressing the files\n"
	gzip -dkfv "$FILE6"

	head "${FILE6%.gz}"

	echo -e "\n\tGenerating IPv4 database “$OUTPUT4”\n"
	python3 -X dev update.py 4 "${FILE4%.gz}" $OUTPUT4

	head $OUTPUT4

	echo -e "\n\tGenerating IPv6 database “$OUTPUT6”\n"
	python3 -X dev update.py 6 "${FILE6%.gz}" $OUTPUT6

	head $OUTPUT6

	echo "| [iptoasn.com](#iptoasncom-database) | 🄍<br>PDDL v1.0 | Country | Daily<br>IPv4: $(outputupdate "$FILE4")<br>IPv6: $(outputupdate "$FILE6") | $(outputfile "$DIR" $OUTPUT4) | $(outputfile "$DIR" $OUTPUT6) |" >table.txt
)

(
	set -e
	trap 'echo "::error::IPinfo.io failed to update"' ERR

	DIR=${DIRS[2]}
	FILE=${FILES[4]}

	echo -e "\nIPinfo.io\n"

	cd "$DIR"
	echo -e "\n\tDownloading “${FILE}”\n"
	wget "${wget_args[@]}" "https://ipinfo.io/data/free/$FILE?token=$TOKEN"
	echo -e "\n\tDecompressing the files\n"
	gzip -dkfv "$FILE"

	FILE=${FILE%.gz}

	headtail "$FILE"

	echo -e "\n\tGenerating IPv4 database “$OUTPUT4”\n"
	python3 -X dev update.py 4 "$FILE" $OUTPUT4

	head $OUTPUT4

	echo -e "\n\tGenerating IPv6 database “$OUTPUT6”\n"
	python3 -X dev update.py 6 "$FILE" $OUTPUT6

	head $OUTPUT6

	echo "| [IPinfo.io](#ipinfoio-databases) | 🅭🅯🄎<br>CC BY-SA 4.0 | Country | Daily<br>$(outputupdate "${FILES[4]}") | $(outputfile "$DIR" $OUTPUT4) | $(outputfile "$DIR" $OUTPUT6) |" >table.txt
)

(
	set -e
	trap 'echo "::error::DB-IP Lite failed to update"' ERR

	DIR=${DIRS[3]}
	FILE=${FILES[5]}

	echo -e "\nDB-IP Lite\n"

	cd "$DIR"
	echo -e "\n\tDownloading “${FILE}”\n"
	wget "${wget_args[@]}" "https://download.db-ip.com/free/$FILE"
	echo -e "\n\tDecompressing the files\n"
	gzip -dkfv "$FILE"

	FILE=${FILE%.gz}

	headtail "$FILE"

	echo -e "\n\tGenerating IPv4 database “$OUTPUT4”\n"
	python3 -X dev update.py 4 "$FILE" $OUTPUT4

	head $OUTPUT4

	echo -e "\n\tGenerating IPv6 database “$OUTPUT6”\n"
	python3 -X dev update.py 6 "$FILE" $OUTPUT6

	head $OUTPUT6

	echo "| [DB-IP Lite](#db-ip-lite-databases) | 🅭🅯<br>CC BY 4.0 | Country | Monthly<br>$(outputupdate "${FILES[5]}") | $(outputfile "$DIR" $OUTPUT4) | $(outputfile "$DIR" $OUTPUT6) |" >table.txt
)

(
	set -e
	trap 'echo "::error::IP2Location LITE failed to update"' ERR

	DIR=${DIRS[5]}
	FILE4=${FILES[7]}
	FILE6=${FILES[8]}

	echo -e "\nIP2Location LITE\n"

	cd "$DIR"

	echo -e "\n\tDownloading “${FILE4}”\n"
	# wget "${wget_args[@]}" -O "$FILE4" "https://www.ip2location.com/download/?token=$TOKEN1&file=DB1LITECSV"
	curl "${curl_args[@]}" -o "$FILE4" "https://www.ip2location.com/download/?token=$TOKEN1&file=DB1LITECSV"
	echo -e "\n\tDecompressing the files\n"
	unzip -o "$FILE4"

	head "${FILE4%.ZIP}"

	echo -e "\n\tDownloading “${FILE6}”\n"
	# wget "${wget_args[@]}" -O "$FILE6" "https://www.ip2location.com/download/?token=$TOKEN1&file=DB1LITECSVIPV6"
	curl "${curl_args[@]}" -o "$FILE6" "https://www.ip2location.com/download/?token=$TOKEN1&file=DB1LITECSVIPV6"
	echo -e "\n\tDecompressing the files\n"
	unzip -o "$FILE6"

	head "${FILE6%.ZIP}"

	echo -e "\n\tGenerating IPv4 database “$OUTPUT4”\n"
	python3 -X dev update.py 4 "${FILE4%.ZIP}" $OUTPUT4

	head $OUTPUT4

	echo -e "\n\tGenerating IPv6 database “$OUTPUT6”\n"
	python3 -X dev update.py 6 "${FILE6%.ZIP}" $OUTPUT6

	head $OUTPUT6

	echo "| [IP2Location LITE](#ip2location-lite-databases) | 🅭🅯🄎<br>CC BY-SA 4.0 | Country | Bimonthly<br>IPv4: $(outputupdate "$FILE4")<br>IPv6: $(outputupdate "$FILE6") | $(outputfile "$DIR" $OUTPUT4) | $(outputfile "$DIR" $OUTPUT6) |" >table.txt
)

(
	set -e
	trap 'echo "::error::GeoLite2 failed to update"' ERR

	DIR=${DIRS[7]}

	echo -e "\nGeoLite2\n"

	cd "$DIR"

	# Database URL
	echo -e "\n\tDownloading “GeoLite2-Country-CSV_*.zip”\n"
	# wget "${wget_args[@]}" "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country-CSV&license_key=$KEY&suffix=zip"{,.sha256}
	curl "${curl_args[@]}" -OOJ "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country-CSV&license_key=$KEY&suffix=zip"{,.sha256}
	if ! sha256sum -c GeoLite2-Country-CSV_*.zip.sha256; then
		echo "Error: sha256sum does not match" >&2
		exit 1
	fi
	echo -e "\n\tDecompressing the files\n"
	unzip -o GeoLite2-Country-CSV_*.zip

	head ${FILES[11]} ${FILES[12]}

	echo -e "\n\tGenerating IPv4 databases “${OUTPUT4%.tsv}*.tsv”\n"
	python3 -X dev update.py ${FILES[11]} ${OUTPUT4} GeoLite2-Country-CSV_*/GeoLite2-Country-Locations-*.csv

	head ${OUTPUT4%.tsv}*.tsv

	echo -e "\n\tGenerating IPv6 databases “${OUTPUT6%.tsv}*.tsv”\n"
	python3 -X dev update.py ${FILES[12]} ${OUTPUT6} GeoLite2-Country-CSV_*/GeoLite2-Country-Locations-*.csv

	head ${OUTPUT6%.tsv}*.tsv

	cp GeoLite2-Country-CSV_*/LICENSE.txt .

	echo "| [GeoLite2](#geolite2-databases) | 🅯🄎<br>GeoLite2 EULA | Country | Weekly<br>IPv4: $(outputupdate ${FILES[11]})<br>IPv6: $(outputupdate ${FILES[12]}) | $(outputfile "$DIR" ${OUTPUT4%.tsv}*.tsv) | $(outputfile "$DIR" ${OUTPUT6%.tsv}*.tsv) |" >table.txt
)

(
	set -e
	trap 'echo "::error::DB-IP Lite failed to update"' ERR

	DIR=${DIRS[4]}
	FILE=${FILES[6]}

	echo -e "\nDB-IP Lite\n"

	cd "$DIR"
	echo -e "\n\tDownloading “${FILE}”\n"
	wget "${wget_args[@]}" "https://download.db-ip.com/free/$FILE"
	echo -e "\n\tDecompressing the files\n"
	gzip -dkfv "$FILE"

	FILE=${FILE%.gz}

	headtail "$FILE"

	echo -e "\n\tGenerating IPv4 database “$OUTPUT4”\n"
	python3 -X dev update.py 4 "$FILE" $OUTPUT4

	head $OUTPUT4

	echo -e "\n\tGenerating IPv6 database “$OUTPUT6”\n"
	python3 -X dev update.py 6 "$FILE" $OUTPUT6

	head $OUTPUT6

	echo "| | | Full Location | Monthly<br>$(outputupdate "${FILES[6]}") | $(outputfile "$DIR" $OUTPUT4) | $(outputfile "$DIR" $OUTPUT6) |" >table.txt
)

(
	set -e
	trap 'echo "::error::IP2Location LITE failed to update"' ERR

	DIR=${DIRS[6]}
	FILE4=${FILES[9]}
	FILE6=${FILES[10]}

	echo -e "\nIP2Location LITE\n"

	cd "$DIR"

	echo -e "\n\tDownloading “${FILE4}”\n"
	# wget "${wget_args[@]}" -O "$FILE4" "https://www.ip2location.com/download/?token=$TOKEN2&file=DB5LITECSV"
	curl "${curl_args[@]}" -o "$FILE4" "https://www.ip2location.com/download/?token=$TOKEN2&file=DB5LITECSV"
	echo -e "\n\tDecompressing the files\n"
	unzip -o "$FILE4"

	head "${FILE4%.ZIP}"

	echo -e "\n\tDownloading “${FILE6}”\n"
	# wget "${wget_args[@]}" -O "$FILE6" "https://www.ip2location.com/download/?token=$TOKEN2&file=DB5LITECSVIPV6"
	curl "${curl_args[@]}" -o "$FILE6" "https://www.ip2location.com/download/?token=$TOKEN2&file=DB5LITECSVIPV6"
	echo -e "\n\tDecompressing the files\n"
	unzip -o "$FILE6"

	headtail "${FILE6%.ZIP}"

	echo -e "\n\tGenerating IPv4 database “$OUTPUT4”\n"
	python3 -X dev update.py 4 "${FILE4%.ZIP}" $OUTPUT4

	head $OUTPUT4

	echo -e "\n\tGenerating IPv6 database “$OUTPUT6”\n"
	python3 -X dev update.py 6 "${FILE6%.ZIP}" $OUTPUT6

	head $OUTPUT6

	echo "| | | Full Location | Bimonthly<br>IPv4: $(outputupdate "$FILE4")<br>IPv6: $(outputupdate "$FILE6") | $(outputfile "$DIR" $OUTPUT4) | $(outputfile "$DIR" $OUTPUT6) |" >table.txt
)

(
	set -e
	trap 'echo "::error::GeoLite2 failed to update"' ERR

	DIR=${DIRS[8]}

	echo -e "\nGeoLite2\n"

	cd "$DIR"

	# Database URL
	echo -e "\n\tDownloading “GeoLite2-City-CSV_*.zip”\n"
	# wget "${wget_args[@]}" "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City-CSV&license_key=$KEY&suffix=zip"{,.sha256}
	curl "${curl_args[@]}" -OOJ "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City-CSV&license_key=$KEY&suffix=zip"{,.sha256}
	if ! sha256sum -c GeoLite2-City-CSV_*.zip.sha256; then
		echo "Error: sha256sum does not match" >&2
		exit 1
	fi
	echo -e "\n\tDecompressing the files\n"
	unzip -o GeoLite2-City-CSV_*.zip

	head ${FILES[13]} ${FILES[14]}

	echo -e "\n\tGenerating IPv4 databases “${OUTPUT4%.tsv}*.tsv”\n"
	python3 -X dev update.py ${FILES[13]} ${OUTPUT4} GeoLite2-City-CSV_*/GeoLite2-City-Locations-*.csv

	head ${OUTPUT4%.tsv}*.tsv

	echo -e "\n\tGenerating IPv6 databases “${OUTPUT6%.tsv}*.tsv”\n"
	python3 -X dev update.py ${FILES[14]} ${OUTPUT6} GeoLite2-City-CSV_*/GeoLite2-City-Locations-*.csv

	head ${OUTPUT6%.tsv}*.tsv

	cp GeoLite2-City-CSV_*/LICENSE.txt .

	echo "| | | Full Location | Weekly<br>IPv4: $(outputupdate ${FILES[13]})<br>IPv6: $(outputupdate ${FILES[14]}) | $(outputfile "$DIR" ${OUTPUT4%.tsv}*.tsv) | $(outputfile "$DIR" ${OUTPUT6%.tsv}*.tsv) |" >table.txt
)

table=''

for dir in "${DIRS[@]}"; do
	pushd "$dir" >/dev/null
	if [[ -e table.txt ]]; then
		table+=$(<table.txt)
		table+=$'\n'
		rm table.txt
	fi
	popd >/dev/null
done

README=$(<README.md.txt)
README=${README/TABLE/$table}

echo -e "\nDatabase comparison table:\n"
echo "$table"

echo "$README" >README.md

(
	OUTPUT4=${OUTPUT4%.tsv}
	OUTPUT6=${OUTPUT6%.tsv}

	shopt -s globstar
	echo "Geolocation databases:"
	wc -l -- **/$OUTPUT4*.tsv **/$OUTPUT6*.tsv
	echo
	du -bch -- **/$OUTPUT4*.tsv **/$OUTPUT6*.tsv
)
